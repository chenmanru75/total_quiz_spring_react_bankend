package com.twuc.webApp.repository;

import com.twuc.webApp.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderRepository extends JpaRepository<Order,Long> {
//    Order findByProduct(Long product);
}
