package com.twuc.webApp.contracts;

public class GetProductResponse {
    private Long id;
    private String name;
    private String unit;
    private Float price;
    private String url;

    public GetProductResponse() {
    }

    public GetProductResponse(Long id, String name, String unit, Float price, String url) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public Float getPrice() {
        return price;
    }

    public String getUrl() {
        return url;
    }
}
