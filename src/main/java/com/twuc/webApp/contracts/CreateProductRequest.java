package com.twuc.webApp.contracts;

public class CreateProductRequest {
    private String name;
    private String unit;
    private Float price;
    private String url;

    public CreateProductRequest() {
    }

    public CreateProductRequest(String name, String unit, Float price, String url) {
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public Float getPrice() {
        return price;
    }

    public String getUrl() {
        return url;
    }
}
