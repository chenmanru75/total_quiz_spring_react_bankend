package com.twuc.webApp.web;

import com.twuc.webApp.contracts.CreateProductRequest;
import com.twuc.webApp.contracts.GetProductResponse;
import com.twuc.webApp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest createProductRequest){
        productService.createProduct(createProductRequest);
        return ResponseEntity.status(HttpStatus.CREATED)
                .build();
    }

    @GetMapping("/{productId}")
    public ResponseEntity<GetProductResponse> queryProduct(@PathVariable Long productId) throws NotExistProduct {
        GetProductResponse getProductResponse = productService.queryProduct(productId);
        return ResponseEntity.ok()
                .body(getProductResponse);
    }

    @GetMapping
    public ResponseEntity<List<GetProductResponse>> getAllProducts(){
        List<GetProductResponse> allProducts = productService.getAllProducts();
        return ResponseEntity.ok()
                .body(allProducts);
    }
}
