package com.twuc.webApp.web;

import com.twuc.webApp.domain.Order;
import com.twuc.webApp.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/addproduct/{productId}")
    public ResponseEntity addProduct(@PathVariable Long productId){
        orderService.addProduct(productId);
        return ResponseEntity.ok()
                .build();
    }

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getOrderList(){
        List<Order> orderList = orderService.getOrderList();
        return ResponseEntity.ok()
                .body(orderList);
    }

    @DeleteMapping("/deleteproduct/{productId}")
    public ResponseEntity deleteProduct(@PathVariable Long productId){
        orderService.deleteProduct(productId);
        return ResponseEntity.ok().build();
    }

}
