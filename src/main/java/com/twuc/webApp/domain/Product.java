package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String unit;

    @Column
    private Float price;

    @Column
    private String url;

    public Product(String name, String unit, Float price, String url) {
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.url = url;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public Float getPrice() {
        return price;
    }

    public String getUrl() {
        return url;
    }
}
