package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String unit;

    @Column
    private Float price;

    @Column
    private Long size;

    @Column
    private Long product_id;

    public Order() {
    }

    public Order(String name, String unit, Float price, Long size, Long product_id) {
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.size = size;
        this.product_id = product_id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getUnit() {
        return unit;
    }

    public Float getPrice() {
        return price;
    }

    public Long getSize() {
        return size;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
