package com.twuc.webApp.service;

import com.twuc.webApp.domain.Order;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductService productService;

    public void addProduct(Long productId) {
        List<Order> allProduct = orderRepository.findAll();
        List<Order> orderList = allProduct.stream().filter(product -> product.getProduct_id() == productId).collect(Collectors.toList());
        System.out.println(orderList.size());

        Product findProduct = productService.findProduct(productId);
        Order order = new Order(
                findProduct.getName(),
                findProduct.getUnit(),
                findProduct.getPrice(),
                1L,
                productId);
        if(orderList.size() == 0){
            orderRepository.save(order);
        }else{
            for(Order o:orderList){
                order.setId(o.getId());
                order.setSize(o.getSize()+1);
                orderRepository.save(order);
            }
        }
    }

    public List<Order> getOrderList() {
        return orderRepository.findAll();
    }

    public void deleteProduct(Long productId) {
        List<Order> allOrder = orderRepository.findAll();
        List<Order> foundOrder = allOrder.stream().filter(product -> product.getProduct_id() == productId).collect(Collectors.toList());
        orderRepository.deleteById(foundOrder.get(0).getId());
    }
}
