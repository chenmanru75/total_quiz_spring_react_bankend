package com.twuc.webApp.service;

import com.twuc.webApp.contracts.CreateProductRequest;
import com.twuc.webApp.contracts.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.repository.ProductRepository;
import com.twuc.webApp.web.NotExistProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public void createProduct(CreateProductRequest createProductRequest) {
        Product product = new Product(
                createProductRequest.getName(),
                createProductRequest.getUnit(),
                createProductRequest.getPrice(),
                createProductRequest.getUrl());
        productRepository.saveAndFlush(product);
    }

    public GetProductResponse queryProduct(Long producId) throws NotExistProduct {
        Optional<Product> found = productRepository.findById(producId);
        if(!found.isPresent()){
            throw new NotExistProduct();
        }
        return new GetProductResponse(
                found.get().getId(),
                found.get().getName(),
                found.get().getUnit(),
                found.get().getPrice(),
                found.get().getUrl()
        );
    }

    public List<GetProductResponse> getAllProducts() {
        List<Product> allProducts = productRepository.findAll();
        List<GetProductResponse> getAllProducts = new ArrayList<>();
        for (Product product : allProducts) {
            getAllProducts.add(
                new GetProductResponse(
                    product.getId(),
                    product.getName(),
                    product.getUnit(),
                    product.getPrice(),
                    product.getUrl()
                )
            );
        }
        return getAllProducts;
    }

    public Product findProduct(Long productId) {
        return productRepository.findById(productId).get();
    }
}
