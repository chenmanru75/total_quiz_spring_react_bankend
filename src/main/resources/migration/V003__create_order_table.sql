CREATE table if not exists orders(
    id BIGINT AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    unit VARCHAR(128) NOT NULL,
    price FLOAT NOT NULL,
    size BIGINT not null,
    product_id BIGINT not null,
    PRIMARY KEY (id)
);