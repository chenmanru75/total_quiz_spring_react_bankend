package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contracts.CreateProductRequest;
import com.twuc.webApp.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderTest extends IntegrationTestBase {
    private MockHttpServletRequestBuilder createProduct(CreateProductRequest product) throws Exception {
        return post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        new ObjectMapper()
                                .writeValueAsString(product));
    }

    @Autowired
    private OrderRepository orderRepository;

    @Test
    void should_return_200_when_add_product_in_order() throws Exception {

        getMockMvc().perform(createProduct(new CreateProductRequest("巧乐兹","个",3.5F,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565068182852&di=1fa40692666cf2f7a191153aa9140dcd&imgtype=0&src=http%3A%2F%2Fp02.sfimg.cn%2F2018%2F4900285873%2Foriginal_4900285873_0_0.jpg")))
                .andExpect(status().is(201));

        getMockMvc().perform(post("/api/addproduct/1"))
                .andExpect(status().is(200))
                .andReturn();
    }

    @Test
    void should_return_all_order_product_when_query_order() throws Exception {
        getMockMvc().perform(createProduct(new CreateProductRequest("巧乐兹","个",3.5F,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565068182852&di=1fa40692666cf2f7a191153aa9140dcd&imgtype=0&src=http%3A%2F%2Fp02.sfimg.cn%2F2018%2F4900285873%2Foriginal_4900285873_0_0.jpg")))
                .andExpect(status().is(201));

        getMockMvc().perform(post("/api/addproduct/1"))
                .andExpect(status().is(200))
                .andReturn();

        getMockMvc().perform(post("/api/addproduct/1"))
                .andExpect(status().is(200))
                .andReturn();

        getMockMvc().perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value("巧乐兹"))
                .andExpect(jsonPath("$[0].size").value(2));
    }

    @Test
    void should_return_200_when_delete_order_product() throws Exception {
        getMockMvc().perform(createProduct(new CreateProductRequest("巧乐兹","个",3.5F,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565068182852&di=1fa40692666cf2f7a191153aa9140dcd&imgtype=0&src=http%3A%2F%2Fp02.sfimg.cn%2F2018%2F4900285873%2Foriginal_4900285873_0_0.jpg")))
                .andExpect(status().is(201));

        getMockMvc().perform(post("/api/addproduct/1"))
                .andExpect(status().is(200))
                .andReturn();

        getMockMvc().perform(delete("/api/deleteproduct/1"))
                .andExpect(status().is(200))
                .andReturn();
        assertEquals(0,orderRepository.findAll().size());
    }
}
