package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contracts.CreateProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductTest extends IntegrationTestBase {

    private MockHttpServletRequestBuilder createProduct(CreateProductRequest product) throws Exception {
        return post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        new ObjectMapper()
                                .writeValueAsString(product));
    }

    @Test
    void should_return_201_when_create_product() throws Exception {
        getMockMvc().perform(createProduct(new CreateProductRequest("巧乐兹","个",3.5F,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565068182852&di=1fa40692666cf2f7a191153aa9140dcd&imgtype=0&src=http%3A%2F%2Fp02.sfimg.cn%2F2018%2F4900285873%2Foriginal_4900285873_0_0.jpg")))
                .andExpect(status().is(201))
                .andReturn();
    }

    @Test
    void should_return_200_and_content_when_query_product() throws Exception {
        getMockMvc().perform(createProduct(new CreateProductRequest("巧乐兹","个",3.5F,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565068182852&di=1fa40692666cf2f7a191153aa9140dcd&imgtype=0&src=http%3A%2F%2Fp02.sfimg.cn%2F2018%2F4900285873%2Foriginal_4900285873_0_0.jpg")))
                .andExpect(status().is(201))
                .andReturn();

        getMockMvc().perform(get(String.format("/api/products/%d", 1L)))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("巧乐兹"))
                .andExpect(jsonPath("$.price").value(3.5));
    }

    @Test
    void should_return_200_and_content_when_get_all_product() throws Exception {
        getMockMvc().perform(createProduct(new CreateProductRequest("巧乐兹","个",3.5F,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565068182852&di=1fa40692666cf2f7a191153aa9140dcd&imgtype=0&src=http%3A%2F%2Fp02.sfimg.cn%2F2018%2F4900285873%2Foriginal_4900285873_0_0.jpg")))
                .andExpect(status().is(201))
                .andReturn();

        getMockMvc().perform(createProduct(new CreateProductRequest("汉堡包","个",20.5F,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565068182852&di=1fa40692666cf2f7a191153aa9140dcd&imgtype=0&src=http%3A%2F%2Fp02.sfimg.cn%2F2018%2F4900285873%2Foriginal_4900285873_0_0.jpg")))
                .andExpect(status().is(201))
                .andReturn();

        getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value("巧乐兹"))
                .andExpect(jsonPath("$[1].name").value("汉堡包"))
                .andReturn();
    }
}
